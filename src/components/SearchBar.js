import React from 'react'

class SearchBar extends React.Component{

    state = {inputTerm:''}

    onFormSubmit=(e)=>{
        e.preventDefault()
        this.props.onSubmit(this.state.inputTerm)
    }

    render(){
        return(
            <div className="ui segment" style={{marginTop:'10px'}}>
                <h3>Search Here</h3>
                <form className="ui form">
                    <div className="field">
                        <label>Search Input:</label>
                        <input type="text" className="searchbar" onChange={(e)=>this.setState({inputTerm:e.target.value})}  placeholder={this.props.placeholder}/>
                    </div>
                    <button onClick={this.onFormSubmit} className="ui button">Search</button>
                </form>
            </div>
        )
    }
}

export default SearchBar