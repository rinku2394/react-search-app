import React from 'react'
import axios from 'axios'

import SearchBar from './SearchBar'
import ImageList from './ImageList'

class App extends React.Component{

    state={imageArray:[]}

    onSearchSubmit=(inputTerm)=>{
        axios.get('https://api.unsplash.com/search/photos',{
            params:{query:inputTerm},
            headers:{
                Authorization: 'Client-ID 045b5531dad8049d544843fd9f4b672f951801f930dd6e4bbe41087998ccb4b3'
            }
        }).then((res)=>{
            this.setState({
                imageArray:res.data.results
            })
        }).catch((err)=>{
            console.log(err)
        })
    }

    render(){
        return(
            <div className="ui container">
                <h2>React Search App</h2>
                <SearchBar onSubmit={this.onSearchSubmit} placeholder="Enter Search Term"/>
                <ImageList imageArr={this.state.imageArray}/>
            </div>
        )
    }
}

export default App