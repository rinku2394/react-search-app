import React from 'react'

import './ImageList.css'

class ImageList extends React.Component{

    renderImages=()=>{
        if(this.props.imageArr.length > 0){
            const images = this.props.imageArr.map((image)=>{
                return <img key={image.id} src={image.urls.small} alt={image.alt_description}/>
            })

            return(
                <div className='image-list'>
                    {images}
                </div>
            )
        }else{
            return(
                <h4 style={{color:'red'}}>No Images Found! Please try again</h4>
            )
        }
    }

    render(){
        return(
            <div className="ui container">
                <h3>Images To Be Render Here:</h3>
                {this.renderImages()}
            </div>
        )
    }
}

export default ImageList